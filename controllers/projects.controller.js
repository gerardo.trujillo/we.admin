const { request, response } = require('express');
const { Project } = require('../models');
const urlSlug = require('url-slug');
const { fileUpload: fileUploadHelper } = require("../helpers");


const projectsGet = async (req, res = response) => {

    const [ total, projects ] = await Promise.all([
        Project.countDocuments({delete:false}),
        Project.find({delete:false}).populate('categories', 'name').populate('kinds', 'name').populate('routes', 'name')
    ]);

    res.json({
        total,
        projects
    });
};

const projectPost = async (req = request, res = response) => {

    let { name, description, language } = req.body;
    const slug = urlSlug(name);
    const existProject = await Project.findOne({slug});
    if (existProject){
        return res.status(400).json({
            msg: `El slug ${slug} no se puede repetir cambiar el nombre de el proyecto`
        })
    }
    let image;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'projects');
    }
    const date = new Date();
    let data = getDataTranslateCreate(language, date, name, description, image);
    const project = new Project(data);

    // Guardar en Base de datos
    await project.save();

    res.json({
        project
    })
}

const projectActive = async (req = request, res = response) => {

    const {id, option} = req.body;

    const [project] = await Promise.all([
        Project.findByIdAndUpdate(id,{active:option}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        project,
        authenticatedUser
    });
}

const projectShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const project = await Project.findById(id).populate('categories').populate('kinds').populate('routes');
    res.status(200).json({
        project
    });
}

const projectPut = async ( req = request, res = response) => {
    const id = req.params.id;
    let { name, description, language } = req.body;
    const projectDB = await Project.findById(id);
    projectDB.categories = [];
    await projectDB.save();
    let image;
    let existImage = false;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'projects');
        existImage = true;
    }

    let data = getDataTranslateUpdate(language, name, description, image, projectDB, existImage );
    const project = await Project.findByIdAndUpdate(id, data, {new: true});
    res.status(201).json({
        project
    });
}

const projectDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [project] = await Promise.all([
        Project.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        project,
        authenticatedUser
    });
}

function getDataTranslateCreate(language, date, name, description, image){
    let data;
    const slug = urlSlug(name);
    let nameComplete = {};
    let descriptionComplete = {};
    if (language == 'es'){
        nameComplete = {
            es: name
        };
        descriptionComplete = {
            es: description
        };
    } else if(language == 'en'){
        nameComplete = {
            en: name
        };
        descriptionComplete = {
            en: description
        };
    }
    data = {
        name: nameComplete,
        slug,
        date,
        description: descriptionComplete,
        image,
    };

    return data;
}


function getDataTranslateUpdate(language, name, description, image, project, exist){
    let data;
    const slug = urlSlug(name);
    let nameComplete = {};
    let descriptionComplete = {};
    if (language == 'es'){
        if (project.name.es && !project.name.en){
            nameComplete = {
                es: name
            }
        } else {
            nameComplete = {
                es: name,
                en: project.name.en
            }

        }
        if (project.description){
            if (project.description.es && !project.description.en){
                descriptionComplete = {
                    es: description
                }
            } else if (!project.description.es && !project.description.en){
                descriptionComplete = {
                    es: description,
                }
            } else {
                descriptionComplete = {
                    es: description,
                    en: project.description.en
                }
            }
        } else {
            descriptionComplete = {
                es: description
            }
        }
    } else if(language == 'en'){
        if (project.name.es && !project.name.en){
            nameComplete = {
                en: name
            }
        } else {
            nameComplete = {
                en: name,
                es: project.name.es
            }

        }
        if (project.description){
            if (project.description.es && !project.description.en){
                descriptionComplete = {
                    en: description
                }
            } else if (!project.description.es && !project.description.en){
                descriptionComplete = {
                    en: description,
                }
            } else {
                descriptionComplete = {
                    en: description,
                    es: project.description.es
                }
            }
        } else {
            descriptionComplete = {
                en: description
            }
        }
    }
    if (exist){
        data = {
            name: nameComplete,
            slug,
            description: descriptionComplete,
            image
        };
    } else {
        data = {
            name: nameComplete,
            slug,
            description: descriptionComplete
        };
    }

    return data;
}

module.exports = {
    projectsGet,
    projectPost,
    projectActive,
    projectShow,
    projectPut,
    projectDelete
}
