const { request, response } = require('express');
const { Task, Area} = require('../models');
const urlSlug = require('url-slug');
const { fileUpload: fileUploadHelper } = require("../helpers");

const tasksGet = async (req, res = response) => {

    const [ total, tasks ] = await Promise.all([
        Task.countDocuments({delete:false}),
        Task.find({delete:false}).populate('user').populate('area')
    ]);

    res.json({
        total,
        tasks
    });
};

const taskPost = async (req = request, res = response) => {

    let { name, price, description, complexity, include, changes, editable, level, version, proposal, size, areaId, language } = req.body;
    const slug = urlSlug(name);
    const existTask = await Task.findOne({slug});
    const area = await Area.findById(areaId);
    const user = req.user;
    if (existTask){
        return res.status(400).json({
            msg: `El slug ${slug} no se puede repetir cambiar el nombre de la tarea`
        })
    }
    let image;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'tasks');
    }
    const date = new Date();
    let data = getDataTranslateCreate(language, date, name, price, area,  description, complexity, include, changes, editable, level, version, proposal, size, image, user);
    const task = new Task(data);

    // Guardar en Base de datos
    await task.save();

    res.json({
        task
    })
}

const taskActive = async (req = request, res = response) => {

    const {id, option} = req.body;

    const [task] = await Promise.all([
        Task.findByIdAndUpdate(id,{active:option}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        task,
        authenticatedUser
    });
}

const taskShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const task = await Task.findById(id).populate('area');
    res.status(200).json({
        task
    });
}

const taskPut = async ( req = request, res = response) => {
    const id = req.params.id;
    let { name, price, description, complexity, include, changes, editable, level, version, proposal, size, areaId, language } = req.body;
    const taskDB = await Task.findById(id);
    let image;
    let existImage = false;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'tasks');
        existImage = true;
    }
    const area = await Area.findById(areaId);
    let data = getDataTranslateUpdate(language, area, name, price, description, complexity, include, changes, editable, level, version, proposal, size, image, taskDB, existImage );
    const task = await Task.findByIdAndUpdate(id, data, {new: true});
    res.status(201).json({
        task
    });
}

const taskDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [task] = await Promise.all([
        Task.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        task,
        authenticatedUser
    });
}

function getDataTranslateCreate(language, date, name, price, area, description, complexity, include, changes, editable, level, version, proposal, size, image, user){
    let data;
    const slug = urlSlug(name);
    let nameComplete = {};
    let complexityComplete = {};
    let descriptionComplete = {};
    let includeComplete = {};
    let changesComplete = {};
    let levelComplete = {};
    let versionComplete = {};
    let proposalComplete = {};
    if (language == 'es'){
        nameComplete = {
            es: name
        };
        complexityComplete = {
            es: complexity
        };
        descriptionComplete = {
            es: description
        };
        includeComplete = {
            es: include
        };
        changesComplete = {
            es: changes
        };
        levelComplete = {
            es: level
        };
        versionComplete = {
            es: version
        };
        proposalComplete = {
            es: description
        };
    } else if(language == 'en'){
        nameComplete = {
            en: name
        };
        complexityComplete = {
            en: complexity
        };
        descriptionComplete = {
            en: description
        };
        includeComplete = {
            en: include
        };
        changesComplete = {
            en: changes
        };
        levelComplete = {
            en: level
        };
        versionComplete = {
            en: version
        };
        proposalComplete = {
            en: description
        };
    }
    data = {
        name: nameComplete,
        date,
        slug,
        price,
        area,
        size,
        editable,
        complexity: complexityComplete,
        description: descriptionComplete,
        include: includeComplete,
        changes: changesComplete,
        level: levelComplete,
        version: versionComplete,
        proposal: proposalComplete,
        image,
        user
    };

    return data;
}


function getDataTranslateUpdate(language, area, name, price, description, complexity, include, changes, editable, level, version, proposal, size, image, task, exist){
    let data;
    let nameComplete = {};
    let descriptionComplete = {};
    let complexityComplete = {};
    let includeComplete = {};
    let changesComplete = {};
    let levelComplete = {};
    let versionComplete = {};
    let proposalComplete = {};
    if (language == 'es'){
        if (task.name.es && !task.name.en){
            nameComplete = {
                es: name
            }
        } else {
            nameComplete = {
                es: name,
                en: task.name.en
            }

        }
        if (task.complexity){
            if (task.complexity.es && !task.complexity.en){
                complexityComplete = {
                    es: complexity
                }
            } else if (!task.complexity.es && !task.complexity.en){
                complexityComplete = {
                    es: intro,
                }
            } else {
                complexityComplete = {
                    es: complexity,
                    en: task.complexity.en
                }
            }
        } else {
            complexityComplete = {
                es: complexity
            }
        }
        if (task.description){
            if (task.description.es && !task.description.en){
                descriptionComplete = {
                    es: description
                }
            } else if (!task.description.es && !task.description.en){
                descriptionComplete = {
                    es: description,
                }
            } else {
                descriptionComplete = {
                    es: description,
                    en: task.description.en
                }
            }
        } else {
            descriptionComplete = {
                es: description
            }
        }
        if (task.include){
            if (task.include.es && !task.include.en){
                includeComplete = {
                    es: include
                }
            } else if (!task.include.es && !task.include.en){
                includeComplete = {
                    es: include,
                }
            } else {
                includeComplete = {
                    es: include,
                    en: task.include.en
                }
            }
        } else {
            includeComplete = {
                es: include
            }
        }
        if (task.changes){
            if (task.changes.es && !task.changes.en){
                changesComplete = {
                    es: include
                }
            } else if (!task.changes.es && !task.changes.en){
                changesComplete = {
                    es: include,
                }
            } else {
                changesComplete = {
                    es: include,
                    en: task.changes.en
                }
            }
        } else {
            changesComplete = {
                es: changes
            }
        }
        if (task.level){
            if (task.level.es && !task.level.en){
                levelComplete = {
                    es: level
                }
            } else if (!task.level.es && !task.level.en){
                levelComplete = {
                    es: level,
                }
            } else {
                levelComplete = {
                    es: level,
                    en: task.level.en
                }
            }
        } else {
            levelComplete = {
                es: level
            }
        }
        if (task.version){
            if (task.version.es && !task.version.en){
                versionComplete = {
                    es: version
                }
            } else if (!task.version.es && !task.version.en){
                versionComplete = {
                    es: version,
                }
            } else {
                versionComplete = {
                    es: version,
                    en: task.version.en
                }
            }
        } else {
            versionComplete = {
                es: version
            }
        }
        if (task.proposal){
            if (task.proposal.es && !task.proposal.en){
                proposalComplete = {
                    es: proposal
                }
            } else if (!task.proposal.es && !task.proposal.en){
                proposalComplete = {
                    es: proposal,
                }
            } else {
                proposalComplete = {
                    es: proposal,
                    en: task.proposal.en
                }
            }
        } else {
            proposalComplete = {
                es: proposal
            }
        }

    } else if(language == 'en'){
        if (task.name.es && !task.name.en){
            nameComplete = {
                en: name
            }
        } else {
            nameComplete = {
                en: name,
                es: task.name.es
            }

        }
        if (task.complexity){
            if (task.complexity.es && !task.complexity.en){
                complexityComplete = {
                    en: complexity,
                    es: task.complexity.es
                }
            } else if (!task.complexity.es && !task.complexity.en){
                complexityComplete = {
                    en: complexity,
                }
            }
        } else {
            complexityComplete = {
                en: complexity
            }
        }
        if (task.description){
            if (task.description.es && !task.description.en){
                descriptionComplete = {
                    en: description,
                    es: task.description.es
                }
            } else if (!task.description.es && !task.description.en){
                descriptionComplete = {
                    en: description,
                }
            }
        } else {
            descriptionComplete = {
                en: description
            }
        }
        if (task.include){
            if (task.include.es && !task.include.en){
                includeComplete = {
                    en: include,
                    es: task.include.es
                }
            } else if (!task.include.es && !task.include.en){
                includeComplete = {
                    en: include,
                }
            }
        } else {
            includeComplete = {
                en: include
            }
        }
        if (task.changes){
            if (task.changes.es && !task.changes.en){
                changesComplete = {
                    en: changes,
                    es: task.changes.es
                }
            } else if (!task.changes.es && !task.changes.en){
                changesComplete = {
                    en: changes,
                }
            }
        } else {
            changesComplete = {
                en: changes
            }
        }
        if (task.level){
            if (task.level.es && !task.level.en){
                levelComplete = {
                    en: level,
                    es: task.level.es
                }
            } else if (!task.level.es && !task.level.en){
                levelComplete = {
                    en: level,
                }
            }
        } else {
            levelComplete = {
                en: level
            }
        }
        if (task.version){
            if (task.version.es && !task.version.en){
                versionComplete = {
                    en: version,
                    es: task.version.es
                }
            } else if (!task.version.es && !task.version.en){
                versionComplete = {
                    en: version,
                }
            }
        } else {
            versionComplete = {
                en: version
            }
        }
        if (task.proposal){
            if (task.proposal.es && !task.proposal.en){
                proposalComplete = {
                    en: proposal,
                    es: task.proposal.es
                }
            } else if (!task.proposal.es && !task.proposal.en){
                proposalComplete = {
                    en: proposal,
                }
            }
        } else {
            proposalComplete = {
                en: proposal
            }
        }
    }
    if (exist){
        data = {
            name: nameComplete,
            price,
            area,
            size,
            editable,
            complexity: complexityComplete,
            description: descriptionComplete,
            include: includeComplete,
            changes: changesComplete,
            version: versionComplete,
            level: levelComplete,
            proposal: proposalComplete,
            image
        };
    } else {
        data = {
            name: nameComplete,
            price,
            area,
            size,
            editable,
            complexity: complexityComplete,
            description: descriptionComplete,
            include: includeComplete,
            changes: changesComplete,
            version: versionComplete,
            level: levelComplete,
            proposal: proposalComplete,
        };
    }

    return data;
}

module.exports = {
    tasksGet,
    taskPost,
    taskActive,
    taskShow,
    taskPut,
    taskDelete
}
