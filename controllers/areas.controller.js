const { request, response } = require('express');
const { Area, Project} = require('../models');
const urlSlug = require('url-slug');
const { fileUpload: fileUploadHelper } = require("../helpers");


const areasGet = async (req, res = response) => {

    const [ total, areas ] = await Promise.all([
        Area.countDocuments({delete:false}),
        Area.find({delete:false}).populate('project')
    ]);

    res.json({
        total,
        areas
    });
};

const areaPost = async (req = request, res = response) => {

    let { name, description, projectId, language } = req.body;
    const slug = urlSlug(name);
    const project = await Project.findById(projectId);
    const existArea = await Area.findOne({slug});
    if (existArea){
        return res.status(400).json({
            msg: `El slug ${slug} no se puede repetir cambiar el nombre de la area`
        })
    }
    const date = new Date();
    let image;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'areas');
    }
    let data = getDataTranslateCreate(language, name, date, description, project, image);
    const area = new Area(data);

    // Guardar en Base de datos
    await area.save();

    res.json({
        area
    })
}

const areaActive = async (req = request, res = response) => {

    const {id, option} = req.body;

    const [area] = await Promise.all([
        Area.findByIdAndUpdate(id,{active:option}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        area,
        authenticatedUser
    });
}

const areaShow = async ( req = request, res = response) => {
    const id = req.params.id;

    const area = await Area.findById(id).populate('project');
    res.status(200).json({
        area
    });
}

const areaPut = async ( req = request, res = response) => {
    const id = req.params.id;
    let { name, description, projectId, language } = req.body;
    const areaDB = await Area.findById(id);
    const project = await Project.findById(projectId);
    await areaDB.save();
    let image;
    let existImage = false;
    if(req.files){
        image = await fileUploadHelper(req.files, undefined, 'areas');
        existImage = true;
    }

    let data = getDataTranslateUpdate(language, name, description, image, project, areaDB, existImage );
    const area = await Area.findByIdAndUpdate(id, data, {new: true});
    res.status(201).json({
        area
    });
}

const areaDelete = async (req = request, res = response) => {
    const id = req.params.id;

    const [area] = await Promise.all([
        Area.findByIdAndUpdate(id,{delete:true}, {new:true})
    ]);

    const authenticatedUser = req.user;

    res.json({
        area,
        authenticatedUser
    });
}

function getDataTranslateCreate(language, name, date, description, project, image){
    let data;
    const slug = urlSlug(name);
    let nameComplete = {};
    let descriptionComplete = {};
    if (language == 'es'){
        nameComplete = {
            es: name
        };
        descriptionComplete = {
            es: description
        };
    } else if(language == 'en'){
        nameComplete = {
            en: name
        };
        descriptionComplete = {
            en: description
        };
    }
    data = {
        name: nameComplete,
        slug,
        description: descriptionComplete,
        date,
        project,
        image,
    };

    return data;
}


function getDataTranslateUpdate(language, name, description, image, project, area, exist){
    let data;
    const slug = urlSlug(name);
    let nameComplete = {};
    let descriptionComplete = {};
    if (language == 'es'){
        if (area.name.es && !area.name.en){
            nameComplete = {
                es: name
            }
        } else {
            nameComplete = {
                es: name,
                en: area.name.en
            }

        }
        if (area.description){
            if (area.description.es && !area.description.en){
                descriptionComplete = {
                    es: description
                }
            } else if (!area.description.es && !area.description.en){
                descriptionComplete = {
                    es: description,
                }
            } else {
                descriptionComplete = {
                    es: description,
                    en: area.description.en
                }
            }
        } else {
            descriptionComplete = {
                es: description
            }
        }
    } else if(language == 'en'){
        if (area.name.es && !area.name.en){
            nameComplete = {
                en: name
            }
        } else {
            nameComplete = {
                en: name,
                es: area.name.es
            }

        }
        if (area.description){
            if (area.description.es && !area.description.en){
                descriptionComplete = {
                    en: description
                }
            } else if (!area.description.es && !area.description.en){
                descriptionComplete = {
                    en: description,
                }
            } else {
                descriptionComplete = {
                    en: description,
                    es: area.description.es
                }
            }
        } else {
            descriptionComplete = {
                en: description
            }
        }
    }
    if (exist){
        data = {
            name: nameComplete,
            slug,
            project,
            description: descriptionComplete,
            image
        };
    } else {
        data = {
            name: nameComplete,
            slug,
            project,
            description: descriptionComplete,
        };
    }

    return data;
}

module.exports = {
    areasGet,
    areaPost,
    areaActive,
    areaShow,
    areaPut,
    areaDelete
}
