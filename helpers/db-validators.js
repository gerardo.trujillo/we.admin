const News = require("../models/news.model");
const { Area, Project, Task, Category, User } = require("../models");

const existEmail = async (email = '') => {
    const existEmail = await User.findOne({email});
    if (existEmail){
        throw new Error(`El correo ${email} ya esta registrado en la base da datos`);
    }
}

const existEmailUpdate = async (email = '', id = '') => {
    const existEmail = await User.findOne({email});
    // const user = await User.findById(id);
    console.log(email + ' El email mandado');
    console.log(id + ' El id mandado');
    console.log(existEmail + ' Usuario encontrado');
    if (existEmail){
        if (existEmail._id != id){
            throw new Error(`El correo ${email} ya esta registrado en la base da datos Actualizado  con el id ${id}`);
        }
    }
}

const existUserForId = async (id) => {
    const existUser = await User.findById(id);
    if (!existUser){
        throw new Error(`El id ${id} no representa a un usuario  en la base da datos`);
    }
}

const allowedCollections = (collection = '', collections = []) => {
    const included = collections.includes(collection);
    if(!included){
        throw new Error(`La collección  ${collection} no esta permita, colecciones validas ${collections} `)
    }
    return true;
}

const existNewsForId = async (id) => {
    const existNews = await News.findById(id);
    if (!existNews){
        throw new Error(`El id ${id} no representa a una noticia  en la base da datos`);
    }
}

const existNewsForSlug = async (slug) => {
    const existNews = await News.findOne({slug});
    if (!existNews){
        throw new Error(`El slug ${slug} no representa a una noticia  en la base da datos`);
    }
}

const existCategoryForSlug = async (slug) => {
    const existCategory = await Category.findOne({slug});
    if (!existCategory){
        throw new Error(`El slug ${slug} no representa a una categoria  en la base da datos`);
    }
}

const existCategorySlug = async (slug) => {
    const existCategory = await Category.findOne({slug});
    if (existCategory){
        throw new Error(`El slug ${slug} no se puede repetir cambiar el nombre de la noticia`);
    }
}

const existCategoryForId = async (id) => {
    const existCategory = await Category.findById(id);
    if (!existCategory){
        throw new Error(`El id ${id} no representa a una categoria  en la base da datos`);
    }
}

const existAreaForSlug = async (slug) => {
    const existArea = awArea.findOne({slug});
    if (existArea){
        throw new Error(`El slug ${slug} no se puede repetir cambiar el nombre del producto`);
    }
}

const existAreaForId = async (id) => {
    const existArea = await Area.findById(id);
    if (!existArea) {
        throw new Error(`El id ${id} no representa a un producto  en la base da datos`);
    }
}

const existProjectForSlug = async (slug) => {
    const existProject = await Project.findOne({slug});
    if (existProject){
        throw new Error(`El slug ${slug} no se puede repetir cambiar el nombre del producto`);
    }
}

const existProjectForId = async (id) => {
    const existProject = await Project.findById(id);
    if (!existProject) {
        throw new Error(`El id ${id} no representa a un producto  en la base da datos`);
    }
}

const existTaskForSlug = async (slug) => {
    const existTask = await Task.findOne({slug});
    if (existTask){
        throw new Error(`El slug ${slug} no se puede repetir cambiar el nombre del producto`);
    }
}

const existTaskForId = async (id) => {
    const existTask = await Task.findById(id);
    if (!existTask) {
        throw new Error(`El id ${id} no representa a un producto  en la base da datos`);
    }
}


module.exports = {
    existEmail,
    existEmailUpdate,
    existUserForId,
    existNewsForSlug,
    existNewsForId,
    allowedCollections,
    existCategoryForSlug,
    existCategoryForId,
    existCategorySlug,
    existTaskForId,
    existTaskForSlug,
    existAreaForId,
    existAreaForSlug,
    existProjectForId,
    existProjectForSlug
}