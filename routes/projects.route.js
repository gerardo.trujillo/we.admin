const { Router} = require('express');
const { check } = require('express-validator');
const { validateFields, validateJWT } = require('../middlewares');
const { existProjectForId, existProjectForSlug } = require("../helpers/db-validators");
const { projectsGet, projectPost, projectPut, projectShow, projectActive, projectDelete } = require("../controllers/projects.controller");

const router = Router();

router.get('/', [
    validateJWT
] , projectsGet);

router.post('/', [
    validateJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validateFields
], projectPost);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProjectForId),
    validateFields
], projectPut);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProjectForId),
    validateFields
], projectShow);


router.post('/active', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProjectForId),
    validateFields
], projectActive);

router.delete('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existProjectForId),
    validateFields
], projectDelete);

module.exports = router;
