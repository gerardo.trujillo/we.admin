const { Router} = require('express');
const { check } = require('express-validator');
const { validateFields, validateJWT } = require('../middlewares');
const { existTaskForId, existTaskForSlug } = require("../helpers/db-validators");
const { tasksGetPublic, tasksGet, taskShowPublic, taskPost,
    taskPut, taskShow, taskActive, taskDesActive, taskDelete,
    tasksChildrenShow, tasksCategoryGetPublic, taskNew } = require("../controllers/tasks.controller");

const router = Router();

router.get('/public', tasksGetPublic);
router.get('/public/:category', tasksCategoryGetPublic);

router.get('/public/show/:slug', [
    check('slug', 'No es un id valido').not().isEmpty(),
    validateFields
], taskShowPublic);

router.get('/', [
    validateJWT
] , tasksGet);

router.post('/', [
    validateJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validateFields
], taskPost);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], taskPut);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], taskShow);

router.get('/children/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], tasksChildrenShow);

router.get('/active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], taskActive);

router.post('/new', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], taskNew);

router.get('/des-active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], taskDesActive);

router.delete('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], taskDelete);

module.exports = router;
