const { Router} = require('express');
const { check } = require('express-validator');
const { validateFields, validateJWT } = require('../middlewares');
const { existAreaForId, existAreaForSlug } = require("../helpers/db-validators");
const { areasGet, areaPost, areaPut, areaShow, areaActive, areaDelete } = require("../controllers/areas.controller");

const router = Router();

router.get('/', [
    validateJWT
] , areasGet);

router.post('/', [
    validateJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validateFields
], areaPost);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existAreaForId),
    validateFields
], areaPut);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existAreaForId),
    validateFields
], areaShow);

router.post('/active', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existAreaForId),
    validateFields
], areaActive);

router.delete('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existAreaForId),
    validateFields
], areaDelete);

module.exports = router;
