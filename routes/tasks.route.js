const { Router} = require('express');
const { check } = require('express-validator');
const { validateFields, validateJWT } = require('../middlewares');
const { existTaskForId, existTaskForSlug } = require("../helpers/db-validators");
const { tasksGet, taskPost, taskPut, taskShow, taskActive, taskDelete } = require("../controllers/tasks.controller");

const router = Router();

router.get('/', [
    validateJWT
] , tasksGet);

router.post('/', [
    validateJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validateFields
], taskPost);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], taskPut);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], taskShow);

router.post('/active', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], taskActive);

router.delete('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existTaskForId),
    validateFields
], taskDelete);

module.exports = router;
