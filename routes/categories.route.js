const { Router} = require('express');
const { check } = require('express-validator');
const { validateFields, validateJWT } = require('../middlewares');
const { existCategoryForId, existCategoryForSlug } = require("../helpers/db-validators");
const { categoriesGetPublic, categoriesGet, categoryShowPublic, categoryPost, categoryPut, categoryShow, categoryActive, categoryDesActive, categoryDelete, categoriesChildrenShow } = require("../controllers/categories.controller");

const router = Router();

router.get('/public', categoriesGetPublic);

router.get('/public/:slug', [
    check('slug', 'No es un id valido').not().isEmpty(),
    check('slug').custom(existCategoryForSlug),
    validateFields
], categoryShowPublic);

router.get('/', [
    validateJWT
] , categoriesGet);

router.post('/', [
    validateJWT,
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validateFields
], categoryPost);

router.put('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existCategoryForId),
    validateFields
], categoryPut);

router.get('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existCategoryForId),
    validateFields
], categoryShow);

router.get('/children/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existCategoryForId),
    validateFields
], categoriesChildrenShow);

router.get('/active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existCategoryForId),
    validateFields
], categoryActive);

router.get('/des-active/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existCategoryForId),
    validateFields
], categoryDesActive);

router.delete('/:id', [
    validateJWT,
    check('id', 'No es un id valido').isMongoId(),
    check('id').custom(existCategoryForId),
    validateFields
], categoryDelete);

module.exports = router;
