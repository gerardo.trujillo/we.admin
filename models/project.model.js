const { Schema, model } = require('mongoose');

const ProjectSchema =  Schema({
    name: {
        type: JSON,
        required: [true, 'El nombre es obligatorio']
    },
    slug: {
        type: String,
        unique: true,
        required: [true, 'El slug es obligatorio']
    },
    date: {
        type: Date,
    },
    description: {
        type: JSON,
    },
    image: {
        type: String,
    },
    images: [{
        type: String
    }],
    active: {
        type: Boolean,
        default: true
    },
    delete: {
        type: Boolean,
        default: false
    }
});

ProjectSchema.methods.toJSON = function () {
    const { __v, ...model } = this.toObject();
    return model;
}

module.exports = model('Project',  ProjectSchema);
