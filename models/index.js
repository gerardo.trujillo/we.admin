const Area = require('./area.model');
const Category = require('./category.model');
const News = require('./news.model');
const Project = require('./project.model');
const Task = require('./task.model');
const ResetPassword = require('./resetPassword.model');
const User = require('./user.model');
const Server = require('./server');

module.exports = {
    Area,
    Category,
    News,
    Project,
    Task,
    ResetPassword,
    User,
    Server
}
