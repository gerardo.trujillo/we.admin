const { Schema, model } = require('mongoose');

const TaskSchema =  Schema({
    name: {
        type: JSON,
        required: [true, 'El nombre es obligatorio']
    },
    slug: {
        type: String,
        unique: true,
        required: [true, 'El slug es obligatorio']
    },
    date: {
        type: Date,
    },
    description: {
        type: JSON,
    },
    complexity: {
        type: JSON
    },
    include: {
        type: JSON
    },
    changes: {
        type: JSON
    },
    editable: {
        type: Boolean
    },
    level: {
        type: JSON
    },
    price: {
        type: Number
    },
    version: {
        type: JSON
    },
    proposal: {
        type: JSON
    },
    size: {
        type: Number
    },
    area: {
        type: Schema.Types.ObjectId,
        ref: 'Area',
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    image: {
        type: String,
    },
    images: [{
        type: String
    }],
    active: {
        type: Boolean,
        default: true
    },
    delete: {
        type: Boolean,
        default: false
    }
});

TaskSchema.methods.toJSON = function () {
    const { __v, ...model } = this.toObject();
    return model;
}

module.exports = model('Task',  TaskSchema);
